package xyz.rflx.bikecomp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.eclipse.paho.client.mqttv3.*
import org.osmdroid.config.Configuration.getInstance
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import xyz.rflx.bikecomp.BuildConfig.mqttPassword
import xyz.rflx.bikecomp.BuildConfig.mqttUsername

class MainActivity : AppCompatActivity() {
    private lateinit var mqttClient: MQTTClient

    private lateinit var notificationManager: NotificationManager

    private lateinit var clipboard: ClipboardManager

    private lateinit var bikeMarker: Marker
    private lateinit var map: MapView
    private lateinit var securityModeButton: FloatingActionButton
    private lateinit var returnToButton: FloatingActionButton
    private lateinit var speedView: TextView
    private lateinit var altitudeView: TextView

    private val bikePoint = GeoPoint(0.0, 0.0)

    private var isSecurityMode = false
    private var isWifiEnabled = false

    companion object {
        const val TAG = "bikecomp"

        const val mqttId = "bikedroid"
        const val mqttServerURI = "tcp://s.rflx.xyz:1883"

        const val topicLat = "bikecomp/lat"
        const val topicLon = "bikecomp/lon"
        const val topicAlt = "bikecomp/alt"
        const val topicSpeed = "bikecomp/spd"
        const val topicAtm = "bikecomp/atm"
        const val topicStolen = "bikecomp/stl"
        const val topicWifi = "bikecomp/wifi"

        const val REQUEST_PERMISSIONS_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        setContentView(R.layout.activity_main)

        clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        notificationManager = ContextCompat.getSystemService(
            this,
            NotificationManager::class.java
        ) as NotificationManager

        createChannel(getString(R.string.bikecomp_notification_channel_id), "bikecomp")

        securityModeButton = findViewById(R.id.securityModeButton)
        returnToButton = findViewById(R.id.returnToButton)
        speedView = findViewById(R.id.speedView)
        altitudeView = findViewById(R.id.altitudeView)

        map = findViewById(R.id.map)
        map.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)

        map.controller.setZoom(15.0)

        returnToButton.setOnClickListener {
            map.controller.animateTo(bikePoint)
        }

        returnToButton.setOnLongClickListener {
            val clip: ClipData = ClipData.newPlainText("coordinates",
                "${bikePoint.latitude}, ${bikePoint.longitude}")
            clipboard.setPrimaryClip(clip)
            Toast.makeText(this, "Coordinates have been copied to the clipboard",
                Toast.LENGTH_SHORT).show()

            true
        }

        securityModeButton.setOnClickListener {
            // Unlock
            if (isSecurityMode) {
                mqttClient.publish(topicAtm, "0", actionListener = object : IMqttActionListener {
                    override fun onSuccess(asyncActionToken: IMqttToken?) {
                        Log.d(TAG, "Successfully sent 0 to $topicAtm")

                        isSecurityMode = false

                        securityModeButton.setImageResource(R.drawable.lock)

                        returnToButton.hide()
                        map.visibility = View.INVISIBLE
                    }

                    override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                        Log.d(TAG, "Cannot send 1 to $topicAtm")
                    }
                })
            }
            // Lock
            else {
                mqttClient.publish(topicAtm, "1", actionListener = object : IMqttActionListener {
                    override fun onSuccess(asyncActionToken: IMqttToken?) {
                        Log.d(TAG, "Successfully sent 1 to $topicAtm")

                        isSecurityMode = true

                        securityModeButton.setImageResource(R.drawable.lock_open)

                        returnToButton.show()
                        map.visibility = View.VISIBLE
                    }

                    override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                        Log.d(TAG, "Cannot send 1 to $topicAtm")
                    }
                })
            }
        }

        securityModeButton.setOnLongClickListener {
            mqttClient.publish(topicWifi, isWifiEnabled.toString(),
                actionListener = object : IMqttActionListener {
                    override fun onSuccess(asyncActionToken: IMqttToken?) {
                        Log.d(TAG, "Successfully sent 1 to $topicAtm")
                        Toast.makeText(this@MainActivity,
                            "The Wi-Fi search mode has been enabled",
                            Toast.LENGTH_SHORT).show()
                    }

                    override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                        Log.d(TAG, "Cannot send 1 to $topicAtm")
                    }
            })
            isWifiEnabled = !isWifiEnabled
            true
        }

        bikeMarker = Marker(map)
        bikeMarker.icon = ContextCompat.getDrawable(this, R.drawable.bike)
        bikeMarker.title = "bike location"
        bikeMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER)

        mqttClient = MQTTClient(this, mqttServerURI, mqttId)

        mqttClient.connect(mqttUsername, mqttPassword, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                Log.d(TAG, "Successfully connected")

                subscribeToTopic(topicLat)
                subscribeToTopic(topicLon)
                subscribeToTopic(topicAlt)
                subscribeToTopic(topicSpeed)
                subscribeToTopic(topicStolen)
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                Log.d(TAG, "Failed to connect. ${exception.toString()}")
            }

        },
            object : MqttCallback {
                override fun connectionLost(cause: Throwable?) {
                    Log.d(TAG, "Connection lost. ${cause.toString()}")
                }

                override fun messageArrived(topic: String?, message: MqttMessage?) {
                    if (isSecurityMode) {
                        when (topic) {
                            topicLat -> bikePoint.latitude = message?.toString()?.toDoubleOrNull()
                                ?: bikePoint.latitude
                            topicLon -> bikePoint.longitude = message?.toString()?.toDoubleOrNull()
                                ?: bikePoint.latitude
                            topicStolen -> stolen()
                        }

                        updateBikeMarker()
                    }
                    else {
                        when (topic) {
                            topicAlt -> altitudeView.text = "${message?.toString()} m"
                            topicSpeed -> speedView.text = "${message?.toString()} km/h"
                        }
                    }
                }

                override fun deliveryComplete(token: IMqttDeliveryToken?) {
                    Log.d(TAG, "Delivery complete")
                }
            })
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val permissionsToRequest = ArrayList<String>()
        var i = 0
        while (i < grantResults.size) {
            permissionsToRequest.add(permissions[i])
            i++
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                this,
                permissionsToRequest.toTypedArray(),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun subscribeToTopic(topic: String) {
        mqttClient.subscribe(topic, actionListener = object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                Log.d(TAG, "Successfully subscribed to $topic")
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                Log.d(TAG, "Cannot subscribe to $topic")
            }
        })
    }

    private fun updateBikeMarker() {
        bikeMarker.position = bikePoint

        if (!map.overlays.contains(bikeMarker)) {
            map.overlays.add(bikeMarker)
        }

        map.invalidate()
    }

    private fun createChannel(channelId: String, channelName: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            )
                .apply {
                    setShowBadge(false)
                }

            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.description = "bikecomp"

            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(notificationChannel)

        }
    }

    private fun stolen() {
        notificationManager.sendNotification(
            "The device just got stolen!",
            this@MainActivity
        )

        map.controller.animateTo(bikePoint)
    }
}
