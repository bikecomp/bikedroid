package xyz.rflx.bikecomp

import android.content.Context
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

class MQTTClient(context: Context?, serverURI: String, clientID: String) {
    private var mqttClient: MqttAndroidClient = MqttAndroidClient(context, serverURI, clientID)

    fun connect(username: String,
                password: String,
                actionListener: IMqttActionListener,
                callback: MqttCallback
    ) {
        mqttClient.setCallback(callback)

        val options = MqttConnectOptions()
        options.userName = username
        options.password = password.toCharArray()
        options.isAutomaticReconnect = true

        try {
            mqttClient.connect(options, null, actionListener)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun subscribe(topic: String, qos: Int = 1, actionListener: IMqttActionListener) {
        try {
            mqttClient.subscribe(topic, qos, null, actionListener)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun unsubscribe(topic: String, actionListener: IMqttActionListener) {
        try {
            mqttClient.unsubscribe(topic, null, actionListener)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun publish(topic: String,
                msg: String,
                qos: Int = 1,
                retained: Boolean = false,
                actionListener: IMqttActionListener
    ) {
        try {
            val message = MqttMessage()
            message.payload = msg.toByteArray()
            message.qos = qos
            message.isRetained = retained
            mqttClient.publish(topic, message, null, actionListener)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun disconnect(actionListener: IMqttActionListener) {
        try {
            mqttClient.disconnect(null, actionListener)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }
}
